/*
 * main.cpp
 *
 *  Created on: Sep 26, 2019
 *      Author: root
 */

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include "memoryFile.h"
#include <iostream>
#include "indexOperate.h"
#include <ctime>
#include <cstdlib>

int main(int argc, char** argv)
{

	/**/
	int size = sizeof(IndexCollectOptStruct) * 12000;
	unsigned seed = time(NULL);
	srand(seed);
	int pos = sizeof(IndexCollectOptStruct);
//	long long top = 0, last = 0;
	initData("test", "/home/test", size, pos);
//	int* list1 = new int[20]{6,6,7,9,7,7,8,9,1,2,3,5,67,4,7,2,1,8,9,2};


	for (int i=0;i<100000;i++){
		int value_num = rand();
		printf("run num: %d \n", i);
		IndexCollectOptStruct st;
		st.sfront = 0;
		st.sback = 0;
		st.status = 1;
		st.key_val = value_num;
		st.dataEnd = value_num;
		st.dataStart = value_num;
		st.indexEnd = value_num;
		st.indexStart = value_num;
//		hasIndexCollect(id, 1111, 0);
//		int p = hasIndexCollect(id, 555555555555, 0);
		findAndWriteGroup("test", (char*)&st);
		readMemoryMap("test", FRIST_POS);
//		HEAD_STRUCT* head = read->readHead();
//		FILE_LEN_TYPE aaa = head->position;
//		FILE_LEN_TYPE top = head->tag.top;
		pos += sizeof(IndexCollectOptStruct);
	}

//	int a = fileCount("test");
	closeGroup("test");

	close_mem();

	return 0;
}


