#include "logFile.h"

PRINT lprint("log.out");

logPrint::logPrint(const char* fileName){
	logPrint::fs = fopen(fileName, "a");
	THREAD trd(&logPrint::log_run, this);
	trd.detach();
}

void logPrint::log_run(){
	while (PRINT::run){
		if (!logPrint::log_queue.empty()){
			logPrint::log_write();
		}
	}
	out_cout--;
}

void logPrint::log_write(){
	int read_count = 10;
	STRING log_txt = EMPTY;
	while (read_count-- > 0 && !logPrint::log_queue.empty()){
		fprintf(logPrint::fs, logPrint::log_queue.front().c_str());
		logPrint::log_queue.pop();
	}
	fflush(logPrint::fs);
}

void logPrint::printFile(STRING format,...){
#ifdef _FILE_PRINT_
	va_list arg_ptr;
	va_start(arg_ptr, format);
	STRING txt = "[MESSAGE]"+timeStr()+" "+format;
	int len = vprintf(txt.c_str(), arg_ptr);
	va_end(arg_ptr);
	va_start(arg_ptr, format);
	char log_str[len];
	vsprintf(log_str, txt.c_str(), arg_ptr);
	STRING str(log_str);
	logPrint::log_queue.push(str);
	va_end(arg_ptr);
#endif
}

void logPrint::printConsole(STRING format,...){
#ifdef _CONSOLE_PRINT_
	va_list arg_ptr;
	va_start(arg_ptr, format);
	STRING txt = "[CONSOLE]"+timeStr()+" "+format;
	vprintf(txt.c_str(), arg_ptr);
	va_end(arg_ptr);
#endif
}

void logPrint::printDebug(STRING format,...){
#ifdef _DEBUG_PRINT_
	va_list arg_ptr;
	va_start(arg_ptr, format);
	STRING txt = "[DEBUG]" + timeStr()+" "+format;
	vprintf(txt.c_str(), arg_ptr);
	va_end(arg_ptr);
#endif
}

void logPrint::print(STRING format,...){
#ifdef _CONSOLE_PRINT_
	va_list arg_ptr;
	va_start(arg_ptr, format);
	STRING txt = timeStr()+" "+format;
	vprintf(txt.c_str(), arg_ptr);
	va_end(arg_ptr);
#endif
}

void logPrint::close(){
	logPrint::run = false;
	while(out_cout > 0){};
	fflush(logPrint::fs);
	fclose(logPrint::fs);
}
