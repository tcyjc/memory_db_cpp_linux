/*

#include <Python.h>
#include "indexOperate.h"
#include "memoryFile.h"

#define _PASTE(a,b) a##b
#define _NAME "MemoryData"
#define MODULE_NAME MemoryData
#define PY_INIT _PASTE(PyInit_,MODULE_NAME)
#define MODULE_ITEMS _PASTE(MODULE_NAME,Modules)
#define MODULE_ITEM _PASTE(MODULE_NAME,Module)
#define CREATEMEMORY_CREATEMEMORY _PASTE(MODULE_NAME,_createMemory)
#define CREATEMEMORY_CLOSEMEMORY _PASTE(MODULE_NAME,_closeMemory)
#define CREATEMEMORY_WRITEMEMORY _PASTE(MODULE_NAME,_writeMemory)
#define CREATEMEMORY_READMEMORY _PASTE(MODULE_NAME,_readMemory)
#define CREATEMEMORY_READBYTE _PASTE(MODULE_NAME,_readByte)
#define CREATEMEMORY_FLUSHMEMORY _PASTE(MODULE_NAME,_flushMemory)
#define CREATEMEMORY_SMALLENDIAN _PASTE(MODULE_NAME,_smallEndian)
#define CREATEMEMORY_WRITEINDEX _PASTE(MODULE_NAME,_writeIndex)


static PyObject* CREATEMEMORY_CREATEMEMORY(PyObject* self, PyObject* args)
{
	const char* file_name;
	int size;
	if (!PyArg_ParseTuple(args, "s|i", &file_name, &size))
		return NULL;
	return (PyObject*)Py_BuildValue("i", createMemory(file_name, size));
}

static PyObject* CREATEMEMORY_CLOSEMEMORY(PyObject* self, PyObject* args)
{
	int id;
	if (!PyArg_ParseTuple(args, "i", &id))
		return NULL;
	closeMemory(id);
	return (PyObject*)Py_BuildValue("i", 1);
}

static PyObject* CREATEMEMORY_WRITEMEMORY(PyObject* self, PyObject* args)
{
	int id;
	char* dest;
	int start;
	int size;
	if (!PyArg_ParseTuple(args, "i|O|i|i", &id, &dest, &start, &size))
		return (PyObject*)Py_BuildValue("i", -1);
//	char* dest_char = (char*)dest;
	writeMemory(id, dest, start, size);
	return (PyObject*)Py_BuildValue("i", 1);
}

static PyObject* CREATEMEMORY_READMEMORY(PyObject* self, PyObject* args)
{
	int id;
	int start;
	int size;
	if (!PyArg_ParseTuple(args, "i|i|i", &id, &start, &size))
		return NULL;
	return (PyObject*)Py_BuildValue("O", readMemory(id, start, size));
}

static PyObject* CREATEMEMORY_READBYTE(PyObject* self, PyObject* args)
{
	int id;
	int start;
	if (!PyArg_ParseTuple(args, "i|i", &id, &start))
		return NULL;
	return (PyObject*)Py_BuildValue("O", readByte(id, start));
}

static PyObject* CREATEMEMORY_FLUSHMEMORY(PyObject* self, PyObject* args)
{
	int id;
	if (!PyArg_ParseTuple(args, "i", &id))
		return NULL;
	flushMemory(id);
	return (PyObject*)Py_BuildValue("i", 1);
}

static PyObject* CREATEMEMORY_SMALLENDIAN(PyObject* self, PyObject* args)
{
	return (PyObject*)Py_BuildValue("i", smallEndian());
}

static PyObject* CREATEMEMORY_WRITEINDEX(PyObject* self, PyObject* args)
{
	int id, pos, top, last;
	char* data;
	if (!PyArg_ParseTuple(args, "i|O|i|i|i", &id, &data, &pos, &top, &last))
		return NULL;
	return (PyObject*)Py_BuildValue("O", writeIndex(id, data, pos, top, last));
}

static PyMethodDef MODULE_ITEMS[] =
{
	{ "createMemory", CREATEMEMORY_CREATEMEMORY, METH_VARARGS },
	{ "closeMemory", CREATEMEMORY_CLOSEMEMORY, METH_VARARGS },
	{ "writeMemory", CREATEMEMORY_WRITEMEMORY, METH_VARARGS },
	{ "readMemory", CREATEMEMORY_READMEMORY, METH_VARARGS },
	{ "flushMemory", CREATEMEMORY_FLUSHMEMORY, METH_VARARGS },
	{ "smallEndian", CREATEMEMORY_SMALLENDIAN, METH_VARARGS },
	{ "readByte", CREATEMEMORY_READBYTE, METH_VARARGS },
	{ "writeIndex", CREATEMEMORY_WRITEINDEX, METH_VARARGS },
	{ NULL, NULL }
};

static struct PyModuleDef MODULE_ITEM =
{
	PyModuleDef_HEAD_INIT,
	_NAME,
	"usage: Memory file operate\n",
	-1,
	MODULE_ITEMS
};

PyMODINIT_FUNC PyInit_MemoryData(void)
{
	return PyModule_Create(&MODULE_ITEM);
}

*/
