#ifndef _HEAD_LOGFILE_H_
#define HEAD_LOGFILE_H_

#define _CONSOLE_PRINT_
//#define _FILE_PRINT_
//#define _DEBUG_PRINT_

#include "common.h"
#define PRINT logPrint


class logPrint{
	private:
		FILE* fs;
		QUEUE<STRING> log_queue;
		bool run = true;
		int out_cout = 1;

	public:
		logPrint(const char*);
		~logPrint(){};
		void printFile(STRING,...);
		void printConsole(STRING,...);
		void printDebug(STRING,...);
		void print(STRING format,...);
		void log_run();
		void log_write();
		void close();
};

extern PRINT lprint;

#endif /* HEAD_LOGFILE_H_ */
