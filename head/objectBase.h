#ifndef _OBJECT_BASE_H
#define _OBJECT_BASE_H

enum obj_stat {close_stat=0, run_stat=1, alive_stat=2, closeing_stat=4};

template <class obj> class object{
	private:
		obj* item = NULL;
	public:
		object(obj*);
		~object();
		obj* get();
		obj* getBase();
		bool isNull() { return NULL == item; };
};

class objectBase{
	public:
		virtual ~objectBase(){};
		virtual void destroy() = 0;
		virtual void create() = 0;
		virtual bool alive() = 0;
};

template <class obj, class base> class valueObject:public object<base>{
	private:
		obj* obj_item = NULL;
	public:
		valueObject(obj* obj_val, base* base_val):valueObject::object(base_val){
			obj_item = obj_val;
		};
		obj* getValue();
};


#endif
