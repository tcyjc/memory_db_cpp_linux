#ifndef HEAD_COMMON_H_
#define HEAD_COMMON_H_

#include <stdio.h>
#include <queue>
#include <mutex>
#include <thread>
#include <set>
#include <vector>
#include <cstring>
#include <stdarg.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <tr1/unordered_map>
#include <exception>
#include <time.h>
#include "objectBase.h"

#define EMPTY ""
#define FILE_STAT struct stat
#define FILE_LOCK struct flock
#define QUEUE std::queue
#define LOCK std::mutex
#define THREAD std::thread
#define SET std::set
#define LIST std::vector
#define MAP std::tr1::unordered_map
#define STRING std::string
#define OBJECT object
#define VOBJECT valueObject
#define TO_STRING std::to_string
#define EXCEPTION std::exception&
#define NEW(item) memset(new item, 0, sizeof(item))
#define CHARS(size) (char*)NEW(char[size])
#define MEM_CPY_CHARS(item) (char*)memcpy(CHARS(sizeof(item)), (char*)&item, sizeof(item))
#define MEM_CRT_CHARS(item, size) (char*)memcpy(CHARS(size), (char*)item, size)
#define FILE_LEN_TYPE long long
#define FILE_RET_TYPE long long
#define HEAD_TYPE unsigned int
#define FILE_INT_TYPE unsigned int
#define LONG_INT long int
#define DESTROY_TIME 2
#define ERR_NUM -1
#define VER 0.99

enum symbol {
	eq = 1,  // ==
    gt = 2,  // >
    lt = 4,  // <
    ic = 8,  // include
};

STRING timeStr(time_t);
STRING timeStr();
extern "C" int fileLock(const char*);
extern "C" int fileUnlock(int);

#endif /* HEAD_COMMON_H_ */
