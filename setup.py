from distutils.core import setup,Extension

MOD = 'MemoryData'
setup(
    name=MOD,
    ext_modules=[
        Extension(name=MOD, sources=['src/pyMemoryFile.cpp', 'src/memoryfile.cpp', 'src/indexOperate.cpp'])
    ],
    include_dirs=['head', 'src', '/usr/include/python3.6dm']
)
